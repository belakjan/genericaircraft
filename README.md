# Flight Control System: Identification and Design of the Control System for Let-L410

In this Semestral work, we will focus on development of the control system for the aircraft called Let-L410.
The task is to identify modes of the given aircraft model in various scenarios and design controls such that the aircraft will behave stable through the predifined flight scenario.
The objective of this task is to minimise tracking error from the preplanned path waypoints, allow for sufficiently fast control responses and compensate for external disturbances.
A basic, but working solution is provided as a part of the assignment. 
This example solution has poor performance and mainly serves as an template for your designs.

<div style="text-align:center">
![aicraft](.fig/Plane.jpg)
</div>

## Installation
The description of the MATLAB/Simulink and FlightGear installations is in this section.

**MATLAB**

Firstly, it is necessary to install MATLAB and Simulink from [here](https://login.mathworks.com/embedded-login/landing.html?cid=getmatlab&s_tid=gn_getml). 
During the installation you will be ask which toolboxes do you want to download.
Please download the tooboxes and blocksets that are stated bellow **at minimun.**

***Toolboxes***
   * Aerospace Blockset           
   * Aerospace Toolbox            
   * Control System Toolbox   
   * DSP System Toolbox    
   * Fixed-Point Designer         
   * Stateflow
   * System Identification Toolbox
   * Motor Control Blockset    
   * Navigation Toolbox   
   * Sensor Fusion and Tracking Toolbox
   * Simulink Design Optimization 
   * Simulink Control Design    
   * UAV Toolbox  
   * Mapping Toolbox (optional)

   * Instrument Control Toolbox
   * Optimization Toolbox
   * Signal Processing Toolbox


**FlightGear Flight Simulator**

The graphs that can be obtained from MATLAB can be hard to accurately interpret for unexperienced control designer.
Thus, we often use simple graphical visualization of the aircraf behavior.
For the purpose of this semester, we recomend the FlightGear simulator, which supports the communication with Simulink. 
To download FlightGear and other necessary files follow notes below.

***Linux***

Under linux environment the FlightGear can be installed using terminal commands (isn't it beautiful?) ...
```bash
sudo add-apt-repository ppa:saiarcot895/flightgear
sudo apt update
sudo apt install flightgear
```
or the FlightGear can be installed using GUI [here](https://linuxhint.com/install-flightgear-flight-simulator-ubuntu/).

***Windows***

Under the windows environment go the official FlightGear [download page](https://www.flightgear.org/download) and follow instructions.


```diff
- Please name the installation folder as FlightGear
```

**Let-L410**

The model of the aircraft, used for the semestral work, can be downloaded from [here](https://wiki.flightgear.org/Let_L-410_Turbolet). ([direct download](http://helijah.free.fr/flightgear/les-appareils/l410/appareil.htm))

***Linux***

On linux we can use terminal commands again...

```bash
wget http://helijah.free.fr/flightgear/tar.gz/l410-25-09-2021.tar.gz -O ~/Download/l410aircraft.tar
cd Download
sudo tar -xvf l410aircraft.tar.gz -C /usr/share/games/flightgear/Aircraft/
```

***Windows***

Under Windows environment extract the aircraft's model and move it to the desired folder to which path is written bellow.
```bash
C:\Program Files\FlightGear\data\Aircraft\
```


## Task overview

You are given a nonlinear model of an aircraf, based on Let-L410NG design, but modified in a way that makes the dynamics undesirable and therefore requires modern control system which will compensate the undesired behaviors ensure the stable flight.

The objective of this semestral work is to design aircraft's autopilot which will be able to perform flight from the take-off to touch-down. 
The mission of the flight is composed of the following states: 
1. Idle (Initial state of the simulation)
2. Take-off (Inital state: zero altitue and airspeed at the start of an runway): 
sucessfull transtion to state 3: Gain altitude 100m and airspeed 70 m/s
3. Low altitude and speed manouvering (2 VOR possitions)
sucessfull transtion to state 4: Heading changed to 180 degrees, altitude 100m and airspeed 70 m/s, distance to VOR < 50m
4. Climb to cruise altitude
sucessfull transtion to state 5: Altitude 5000m and airspeed 150 m/s, distance to VOR < 50m
5. High altitude and speed manouvering (2 VOR possitions)
sucessfull transtion to state 6: Heading changed to 0 degrees, altitude 5000m and airspeed 150 m/s, distance to VOR < 50m
6. Approach to Airfield
sucessfull transtion to state 7: Altitude 1000m and Airspeed 70 m/s, distance to VOR < 50m
7. Airfield localiser capture
sucessfull transtion to state 8: distance to LOC <10m, Altitude 500m and Airspeed 70 m/s
8. Glide slope
sucessfull transtion to state 9: Glide slope 3 degrees, Altitude 22 m, Airspeed 70 m/s
9. Flare
sucessfull transtion to state 10: Altitude 1 m, Airspeed 50 m/s, descent race < 1m/s
10. Landing
sucessfull completation: Airspeed 0 m/s , Altitude 0m/s 

<!-- The primary objective of this semestral work is to design various control system capable of modifying the aircraft modes, allow direct control of an aicraft states and mitigate the impact of external disturbances. 

The flight and dynamics control toolbox will be utilized for generating signals from VOR (VHF Omnidirectional Range) and ILS (Instrument Landing System) radio-navigation systems. 
These signals will be instrumental in configuring the resulting model in Simulink, a powerful simulation platform. -->


## Task assignment

The provided low-performance solution consists of:

* Generic Plane simulink model
* Very simple autopilot (The goal of this semestral work is to design a much better one)
* FlightGear visualization block 

The solution produced by this approach has very low performance and will not be able to perform whole flight.
To improve the solution, you can follow the steps sugested during the lectures and possibly find your way to improve the solution.

***Recommended approach:***

1. Investigate all aircraft's modes in *Longitudinal dynamics* fot the expected flight scenario
2. Investigate all aircraft's modes in  *Lateral dynamics* the expected flight scenario
3. Design *Pitch hold autopilot* using linearized model of the aircraft
    * Use root-locus method and/or LQR design 
      * Validate with gain and phase margins
    * Include a pitch rate damper
    * Ensure robust attitude tracking capabilites
    * Quantificate response to the pilot/autopilot commands
    * Validate your design using nonlinear model of the aircraft
4. Design *Altitude select/hold* using previous design of the pitch attitude hold autopilot
    * Include Pitch attitude hold in the design procedure
    * Ensure zero steady state error of step changes in altitude
    * Validate with phase and gain margin
    * Validate your design using the nonlinear model of the aircraft
5. Design *Roll attitude hold autopilot* using linearized model of the aircraft
    * Use root-locus method and/or LQR design 
      * Validate with gain and phase margins
    * Investigate the usage of roll rate damper.
    * Ensure robust attitude tracking capabilites
    * Quantificate response to the pilot/autopilot commands
    * Validate your design using nonlinear model of the aircraft
6. Design *Heading select/hold* using previous design of the roll autopilot
    * Include Roll attitude hold in the design procedure
    * Ensure zero steady state error of step changes in heading
    * Validate with phase and gain margin
    * Validate your design using the nonlinear model of the aircraft
7. Design *Airspeed controller*
    * Take into account change of the airspeed in different states of the flight
    * Ensure zero steady state error of step changes in airspeed
    * Ensure airspeed remains above stall speed during a flight (with sufficient margin)
    * 
8. Design Instrument Landing System [5]
    * Design *Glide Slope tracking autopilot*
    * Design automatic *Flare maneuver* algorithm 
9. Verify your design by performing the whole flight

Although we prepared a skeleton solution as a baseline, **feel free to design your algorithms to improve the overall performance**.

## How to begin
Begin with running *Genericaircraft.prf*. 
It will execute the *Init.m* file automatically and ask for another actions using dialog windows.

If the *FlightGear* is runnning the view can be changed pressing 'v'. 
Also using key 'h' might be useful.
Other FlighGear's shortcuts can be found [here](https://wiki.flightgear.org/Keyboard_shortcuts).

## Evaluation

The deadline of the semestral work is 14th week of the semestr, when you will show you results in the labs in order to be given "zápočet".
The results will then be presented during the final examination in a form of a short presentation (up to 15 minutes).

**Common criteria**
  * Modes of the aircraft must be accuratelly identified and described in the presentation.
  * The presentation will include the graphs validating your proposed system (step responses, safety margins, designed architecture).
 * Controllers should be designed in a way that minimalizes rise time, while ensures zero or small overshoot (typically 10% overshoot is fine but discuss it with the teacher to make sure). 

**Things to avoid**

 * ***Do not change aircrafts model (*GenericPlane.slx*) in any case! ANY MODIFICATIONS WILL NOT BE EXCEPTED ***




## Troubleshooting

**Updating the repository**

If there is an update in the repository, you can pull it to your local machine using git:
```
cd ${HOME}/git/genericaircraft && git pull
```

**Contacts**

If you find a bug in the task, you need assistance, or you have any other questions, please contact by email one of (or all of):

* Jan Belak `belakjan@fel.cvut.cz`
* Ondrej Prochazka `prochon4@fel.cvut.cz`

We will try to help you as soon as possible.

## Disclaimer

During the semestr we reserve the right to modify the git version to fix any errors.

But a student has a right to use any version available from the begining of the semester to complete the semestral work.

## References
* [1] Michal Neuer - Bakalářská práce, Letové tratě a jejich provozní [specifikace](specifikace) [online](https://dk.upce.cz/bitstream/handle/10195/39411/NeuerM_Letove%20trate_DS_2011.pdf?sequence=1)
* [2] [https://wiki.flightgear.org/Command_line_options](https://wiki.flightgear.org/Command_line_options)
* [3] [https://dev.px4.io/v1.11_noredirect/en/simulation/flightgear.html](https://dev.px4.io/v1.11_noredirect/en/simulation/flightgear.html)
* [4] [https://history.nasa.gov/SP-4404/ch8-3.htm](https://history.nasa.gov/SP-4404/ch8-3.htm) Ben R. Rich, "Lockheed CL-400 Liquid Hydrogen-Fueled Mach 2.5 Reconnaissance Vehicle," read at a symposium on hydrogen-fueled aircraft, NASA Langley Research Center, 15-16 May 1973.
* [5] [https://www.aai.aero/en/content/what-ils-and-its-different-component](https://www.aai.aero/en/content/what-ils-and-its-different-component)
