% %% evaluation file for flight mission control system semestral task
% % @author: Ondřej Procházka

if exist('out','var')
    %% Compare predefined path with flown trajectory
    f1 = figure();
    nexttile
    hold on
    plot(sem_setup.waypointsInNED(:,1), sem_setup.waypointsInNED(:,2),'.','Markersize',12);
    axis equal
    plot(out.NED.Data(:,1), out.NED.Data(:,2))
    for x= 1:length(sem_setup.waypointsInNED)
        circle(sem_setup.waypointsInNED(x,1), sem_setup.waypointsInNED(x,2),100);
    end
    grid on
    xlabel('x [m]')
    ylabel('y [m]')
    legend('Reference waypoints','Flight trajectory')

    nexttile
    plot(out.tout, out.h_ref.Data)
    hold on
    plot(out.tout, -1*out.NED.Data(:,3))
    grid on
    title('Altitude')
    xlabel('time [s]')
    ylabel('altitude [m]')
    legend('Reference trajectory','Flight trajectory')

    nexttile
    plot(out.tout, out.v_ref.Data)
    hold on
    plot(out.tout, out.v_m.Data)
    grid on
    title('Airspeed')
    xlabel('time [s]')
    ylabel('airspeed [m\cdot s^{-1}]')
    legend('Reference trajectory','Flight trajectory')

    nexttile
    plot(out.tout, (out.dir_ref.Data));
    hold on
    plot(out.tout, (out.dir_m.Data))
    grid on
    title('Heading [rad]')
    xlabel('time [s]')
    ylabel('heading [rad]')
    legend('Reference trajectory','Flight trajectory')

    %% Plot results in 3D
    f2 = figure();
    hold on
    axis equal
    scatter3(sem_setup.waypointsInNED(:,1), sem_setup.waypointsInNED(:,2), sem_setup.waypointsInNED(:,3), 'o');
    plot3(out.NED.Data(:,1), out.NED.Data(:,2), -1*out.NED.Data(:,3));
    grid on
    xlabel('x [m]')
    ylabel('y [m]')
    zlabel('z [m]')
    legend('Reference waypoints','Flight trajectory')
    [caz,cel] = view([7 2 5]);
    

    %% Fancy visualization (Windows only)
    % Mapping Toolbox is required 
    % source: https://www.mathworks.com/help/map/visualize-uav-flight-path-on-synchronized-maps.html
    if isunix
        
        fprintf('You can try to fix it: https://www.mathworks.com/matlabcentral/answers/157894-resolving-low-level-graphics-issues-in-matlab\n');
    
        
    elseif ispc
        if license('test','map_toolbox')

            latitude =  out.LLA.Data(:,1);
            longitude = out.LLA.Data(:,2);
            altitude = out.LLA.Data(:,3);
   
            warning off
            figpos = [0 0 1920 1800];
            uif = uifigure("Position",figpos);
            ug = uigridlayout(uif,[1,2]);
            p1 = uipanel(ug);
            p2 = uipanel(ug);
            gx = geoaxes(p1,"Basemap","topographic"); 
            gg = geoglobe(p2); 

            geoplot3(gg,latitude,longitude,altitude-altitude(1),"c","LineWidth",2,"HeightReference","terrain")
            hold(gg,"on");
            geoplot3(gg,lla(:,1),lla(:,2),(lla(:,3) + -1*CoordinateCenterNED.altitude)*-1,"g","LineWidth",2,"HeightReference","terrain")

            ptrack = geoplot(gx,latitude,longitude,"c","LineWidth",2);
            hold(gx,"on");
            ptrack = geoplot(gx,lla(:,1),lla(:,2),'--gs',"LineWidth",2, "MarkerSize",10,"MarkerEdgeColor","b", ...
                "MarkerFaceColor",[0.5 0.5 0.5]);
            warning on
        end
    end
end

%% useful functions

function h = circle(x,y,r)
th = 0:pi/50:2*pi;
xunit = r * cos(th) + x;
yunit = r * sin(th) + y;
h = plot(xunit, yunit,'--b');
end