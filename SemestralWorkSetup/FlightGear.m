%% run FlightGear
answer = questdlg('Would you like to run FlightGear?', ...
    'Run FlightGear', ...
    'Yes','No','Yes');

% Handle response
switch answer
    case 'Yes'
        set_param('SemestralWork/FlightGearVisualization/FlightGear_sent','commented','off')
        save_system('FlightGearVisualization')
        disp('INFO: Running FG.')
        if ismac
            % Code to run on Mac platform
            system('SemestralWorkSetup/runfg-osx.sh &');
        elseif isunix
            system('SemestralWorkSetup/runfg.sh &');
            % TODO: needs to be solved later, can be run dirrectly from the
            % terminal
        elseif ispc
            system('SemestralWorkSetup\runfg.bat &');
        else
            disp('Platform not supported')
        end
    
    case 'No'
        set_param('SemestralWork/FlightGearVisualization/FlightGear_sent','commented','on')
        save_system('FlightGearVisualization')
        disp('INFO: Simmulation runs without FlightGear!') 
end
clear answer