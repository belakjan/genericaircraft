% %% definition file for flight mission control
% % @author: Ondřej Procházka

Waypoints = [
    0,0, 0; ...
    1500, 0, 100; ...
    1500+7000, 1.5*7000, 100; ...
    500, 3*7000, 500; ...
    -30000, 4*7000, 5000; ...
    -38500-7000, 15000, 5000; ...
    -34500, -1000, 3000; ...
    -10500, 0, 1000; ...
    -10600, 0, 500; ...
    -5500, 0, 290; ...
    0, 0, 0;
    ];
% 
% Waypoints = [
%     0,0, 0; ...
%     1500, 0, 100; ...
%     1500+7000, 1.5*7000, 100; ...
%     500, 3*7000, 500; ...
%     -30000, 4*7000, 5000; ...
%     -38500-7000, 15000, 5000; ...
%     -34500, -1000, 3000; ...
%     -5500, 0, 1000; ...
%     -5600, 0, 1000; ...
%     -1500, 0, 500;
%     ];

sem_setup.airspeed = [70,70,70,150,150,70,70,70,70,0];
sem_setup.wind = [0,0,0;
                  0,0,0;
                  0,0,0;
                  10,0,0;
                  0,0,0;
                  1,0,0;
                  1,0,0;
                  1,0,0;
                  1,0,1/10;
                  0,0,-1/10;
                  0,0,0;
                  0,0,0;
                  0,0,0;];
              
%% Vaclav Havel Airport runway
CoordinateCenterNED.latitude = 50.0942250; % initial spawn position
CoordinateCenterNED.longitude = 14.2739253; 
CoordinateCenterNED.altitude = -366;
RunwayHeading = deg2rad(-53); % Pragur Vaclav Havel Airport

% define at which state simulation start
sem_setup.sim_start.BodyV = [20,0,0];
sem_setup.sim_start.height = 0;
sem_setup.sim_start.heading = RunwayHeading;

%% Rotate waypoints to align it with runway's heading
R = [
    cos(abs(RunwayHeading)) -sin(abs(RunwayHeading)) 0; 
    sin(abs(RunwayHeading)) cos(abs(RunwayHeading)) 0;
    0 0 1
    ];

Waypoints = (R*Waypoints')';

headings = atan2(Waypoints(2,2)-Waypoints(1,2), Waypoints(2,1)-Waypoints(1,1))*-1;

for i=2:(size(Waypoints,1)-1)
    headings = [headings; atan2(Waypoints(i+1,2)-Waypoints(i,2), Waypoints(i+1,1)-Waypoints(i,1))*-1];
end
clear i

%% Recompute from NED to LLA
llaInit = [CoordinateCenterNED.latitude, CoordinateCenterNED.longitude, CoordinateCenterNED.altitude];
sem_setup.waypointsInNED = [Waypoints(:,1), Waypoints(:,2)*-1 , Waypoints(:,3)];
lla = ned2lla(sem_setup.waypointsInNED, llaInit, 'flat');

%% Visualization
if isunix
    
    plot(lla(:,2), lla(:,1),  '-x')
    axis equal
    xlabel('longitude');
    ylabel('latitude');
    title('Visualization of the planned waypoints.');
    grid on
    
elseif ispc
    if ~license('test','map_toolbox')
        
        plot(lla(:,2), lla(:,1),  '-x')
        axis equal
        xlabel('longitude');
        ylabel('latitude');
        title('Visualization of the planned waypoints.');
        grid on
    
    else
        
        % Mapping Toolbox is required 
        % source: https://www.mathworks.com/help/map/visualize-uav-flight-path-on-synchronized-maps.html
        warning off
        figpos = [0 0 1920 1800];
        uif = uifigure("Position",figpos);
        ug = uigridlayout(uif,[1,2]);
        p1 = uipanel(ug);
        p2 = uipanel(ug);
        gx = geoaxes(p1,"Basemap","topographic"); 
        gg = geoglobe(p2); 

        geoplot3(gg,lla(:,1),lla(:,2),(lla(:,3) + -1*CoordinateCenterNED.altitude)*-1,"g","LineWidth",2,"HeightReference","terrain")
        hold(gx,"on")

        ptrack = geoplot(gx,lla(:,1),lla(:,2),'--gs',"LineWidth",2, "MarkerSize",10,"MarkerEdgeColor","b", ...
        "MarkerFaceColor",[0.5 0.5 0.5]);

        title(gx,"Position of the planned waypoints.")
        clear ptrack gg gx p1 p2 ug uif figpos
        warning on
    end
end

sem_setup.waypoints = Waypoints;
sem_setup.headings = headings;
clear theta R Waypoints WaypointsInNED llaInit RunwayHeading headings