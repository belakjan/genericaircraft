C:
cd "C:\Program Files\FlightGear"

SET FG_ROOT=C:\Program Files\FlightGear\data\

.\\bin\fgfs --lon=14.263181099999997 --lat=50.10179100000001 --altitude=1194 --fdm=null --enable-auto-coordination --native-fdm=socket,in,30,localhost,5502,udp --fog-disable --enable-clouds3d --start-date-lat=2004:06:01:09:00:00 --enable-sound --visibility=15000 --in-air --prop:/engines/engine0/running=true --disable-freeze --disable-fuel-freeze --disable-fgcom --heading=0 --offset-distance=0 --offset-azimuth=0 --enable-rembrandt --timeofday=noon --enable-terrasync --aircraft=l410