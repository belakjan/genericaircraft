listing = dir('trim\precomputed_scenarios');
for i = 1:length(listing)
    if ~contains(listing(i).name,'.mat')
        continue
    end
    load(listing(i).name)
    LINsys =Linearize_plane();
    save(listing(i).name,"plane","LINsys");
    movefile(listing(i).name,'trim\precomputed_scenarios');
end