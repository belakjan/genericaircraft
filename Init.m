% %% initial file for flight control system semestral task
% % @author: Jan Belák

% Determine where your m-file's folder is.
folder = fileparts(which(mfilename)); 

% Add that folder plus all subfolders to the path.
addpath(genpath(folder));
clear folder

% Configuration file
Airframe;

Aerodynamic;

Powertrain;

AerodynamicSurfaces;

Initial;

Trim;

MissionControl;

open('SemestralWork')

FlightGear;

