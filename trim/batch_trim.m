airspeed = [50,70,90,110,150];
Altitude = [100,1000,5000];
Flaps_state = [0,1];
for i = 1:length(airspeed)
    for j = 1:length(Altitude)
        for k = 1:length(Flaps_state)
            [plane,error] = TrimFC(plane,airspeed(i), Altitude(j),Flaps_state(k));
            if error <1e-4
                if Flaps_state(k)
                    ending = "_F";
                else
                    ending = "";
                end
                name = "V_"+airspeed(i)+"_Alt_"+Altitude(j)+ending;
                save(name,"plane");
                movefile(name+".mat",'trim\precomputed_scenarios');
            end
        end
    end
end
