%% Airframe characteristics of L 410 NG

plane.Airframe.S = 19.478;                                  % [m]
plane.Airframe.A = 34.86;                                   % [m^2]
plane.Airframe.MAC = plane.Airframe.A/plane.Airframe.S;     % mean aerodynamic chord
plane.Airframe.Mass = 4000;                                 % [kg]
plane.Airframe.CG = [0,0,0]';                               % center of gravity
plane.Airframe.CP = [0,0,0]';                               % center of pressure
plane.Airframe.I = [4,0,-0.14;0,3.6,0;-0.14,0,9.55]*4000;   % inertia